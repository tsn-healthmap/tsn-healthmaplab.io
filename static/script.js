// L.mapbox.accessToken = 'pk.eyJ1IjoidHNuLWhlYWx0aG1hcCIsImEiOiJja2t1ZTJkYjEwc2ljMndtc2NzeXF2cDAwIn0.NHVrYDLIA7_a_E5fqTLUoQ';

var mymap = L.map('mapid')
L.tileLayer("https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidHNuLWhlYWx0aG1hcCIsImEiOiJja2t1Zmh5cnAwMDZiMnVvNGVqb3AzZXBnIn0.IQJG0LsXu_KU550_yTY12A", {
	accessToken: "pk.eyJ1IjoidHNuLWhlYWx0aG1hcCIsImEiOiJja2t1Zmh5cnAwMDZiMnVvNGVqb3AzZXBnIn0.IQJG0LsXu_KU550_yTY12A",
	 attribution: 'Map Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1}).addTo(mymap);
const fillo = 0.5;
const lweight = 2;
mymap.setView([54.505, -1], 5);

var scot = new L.GeoJSON.AJAX("/healthmap/saved/scotland.geojson", 
	{style:{
		"weight":lweight,
		"color":"#ccff00",
		"fillOpacity":fillo
	},
	onEachFeature: (feature,layer)=>{
		layer.bindPopup('<h1>'+feature.properties.HBName+'</h1><p>HBCode: '+feature.properties.HBCode+'</p>');
	}});
var england = new L.GeoJSON.AJAX("/healthmap/saved/england.geojson",
	{"style":{
		"weight": lweight,
		"color":"#00FFFF",
		"fillOpacity":fillo,
	},
		"onEachFeature": (feature,layer)=>{
			layer.bindPopup('<h1>'+feature.properties.ccg18nm+'</h1><p>CCG Code: '+feature.properties.ccg18cd+'</p>');
	}});
var ni = new L.GeoJSON.AJAX("/healthmap/saved/northern_ireland.geojson",
	{style:{
		"weight":lweight,
		"color":"#FF00FF",
		"fillOpacity":fillo,
	},
		"onEachFeature": (f,l)=>{
			l.bindPopup('<h1>'+f.properties.TrustName+'</h1><p>Trust Code: '+f.properties.TrustCode+'</p>');
		}
	});
var wales = new L.GeoJSON.AJAX("/healthmap/saved/wales.geojson",
	{style:{
		"weight":lweight,
		"color":"#39ff14",
		"fillOpacity":fillo,
	},
		"onEachFeature": (f,l)=>{
                        l.bindPopup('<h1>'+f.properties['Welsh Name']+"/"+f.properties['English Name']+'</h1><p>Area Code: '+f.properties['Area Code']+'</p>');

		}
	});
scot.addTo(mymap);
england.addTo(mymap);
ni.addTo(mymap);
wales.addTo(mymap);
